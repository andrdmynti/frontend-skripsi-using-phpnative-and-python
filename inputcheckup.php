<?php

	session_start();
	
	$namabidan    = $_SESSION['name'];

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<style>

	  body {
	      font-family: "Lato", sans-serif;
	  }

	  .sidenav {
	      height: 100%;
	      width: 0;
	      position: fixed;
	      z-index: 1;
	      top: 0;
	      left: 0;
	      background-color: white;
	      overflow-x: hidden;
	      transition: 0.5s;
	      padding-top: 0px;
	  }

	  .sidenav a {
	      padding: 8px 8px 8px 32px;
	      text-decoration: none;
	      font-size: 25px;
	      color: #818181;
	      display: block;
	      transition: 0.3s;
	  }

	  .sidenav a:hover {
	      color: #f1f1f1;
	  }

	  .sidenav .closebtn {
	      position: absolute;
	      top: 0;
	      right: 25px;
	      font-size: 36px;
	      margin-left: 50px;
	  }

	  @media screen and (max-height: 450px) {
	    .sidenav {padding-top: 15px;}
	    .sidenav a {font-size: 18px;}
	  }
	  </style>
	  <style type="text/css">
	    * {
	      padding:0;
	      margin:0;
	    }

	    body {
	      font-family:Verdana, Geneva, sans-serif;
	      font-size:18px;
	      background-color:#FFF
	    }

	    input.untukInput1 {
	      border-bottom: 1px solid #2b2a2a;
	      border-left:none;
	      border-right:none;
	      border-top:none;
	    }

	    select.untukInput2 {
	      border-bottom: 1px solid #2b2a2a;
	      border-left:none;
	      border-right:none;
	      border-top:none;
	    }

	    header {
	      width:100%;
	      background-color:#006faa ;
	      z-index:1000;
	    }

	    .menu-bar {
	      color:white;
	      font-size:25px;
	      cursor:pointer;
	      padding:10px 12px;
	      margin-left:10px;
	      margin-top:5px;
	      margin-bottom:5px;
	    }

	    .menu-bar:hover {
	      background-color:rgba(0, 0, 0, 0.1);
	      border-radius:50px;
	    }

	    #tag-menu {
	      display:none;
	    }


	    #tag-menu:checked ~ div.jw-drawer {
	     animation: slide-in 0.5s ease;
	     animation-fill-mode: forwards;
	    }

	    .jw-drawer {
	      position:fixed;
	      left:-280px;
	      background-color:#006faa;
	      height:100%;
	      z-index:100;
	      width:230px;
	      animation: slide-out 0.5s ease;
	      animation-fill-mode: forwards;
	    }

	    .jw-drawer ul li {
	      list-style:none;
	    }

	    .jw-drawer ul li a {
	      padding:10px 20px;
	      text-decoration:none;
	      display:block;
	      color:#FFF;
	      /*border-top:1px solid #059;*/
	    }

	    .jw-drawer ul li a:hover{
	      background-color:rgba(0, 0, 0, 0.1);
	    }

	    .jw-drawer ul li a i {
	      width:50px;
	      height:35px;
	      text-align:center;
	      padding-top:15px;
	    }

	    @keyframes slide-in {
	     from {left: -280px;}
	     to {left: 0;}
	    }

	    @keyframes slide-out {
	     from {left: 0;}
	     to {left: -280px;}
	    }
	</style>
</head>
<body>
	<header>
	  <input type="checkbox" id="tag-menu"/>
	  <label onclick="openNav()"  class="fa fa-bars menu-bar" for="tag-menu"></label><font color="white">Input Hasil Checkup Pasien</font>
	  
	  <div id="mySidenav" class="sidenav">
	    <header>
	      <br>
	      <br>
	      <center>
	        <img src="images/bidandelima.png" height="70" width="70">
	      </center>
	      <font size="2" color="white">
	          <br>
	          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hai! Bidan <?php echo $_SESSION['name']; ?>
	          <br>
	          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $_SESSION['email']; ?>
	          <br>
	      </font>
	      <br>
	    </header>
	    
	      <br>
	      <a href="datadiripasien.php"><i class="fa fa-user"></i>&nbsp;&nbsp;<font size="2">Data Diri</font></a>
			    <a href="hasilcheckup.php"><i class="fa fa-file"></i>&nbsp;&nbsp;<font size="2">Hasil Checkup</font></a>
			    <a href="artikel.php"><i class="fa fa-newspaper-o"></i>&nbsp;&nbsp;<font size="2">Artikel</font></a>
                      <a href="report.php"><i class="fa fa-newspaper-o"></i>&nbsp;&nbsp;<font size="2">Report</font></a>
			    <a href="logout.php"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;<font size="2">Logout</font></a>
	  </div>
	</header>
	<div class="container">
		<br>
		<br>
		<form class="col-sm-12" action="proses_inputcheckup.php" method="POST" >
			<div class="form-group col-sm-10">
				<?php
					include 'koneksi.php';
					$bidan 		= "SELECT * FROM bidan WHERE nama_bidan='$namabidan'";
					$queryBidan = mysqli_query($conn,$bidan);
					$dataBidan  = mysqli_fetch_array($queryBidan);
				?>
				<!-- <input class="form-control untukInput2" type="text" name="bidan"> -->
				<select class="form-control untukInput2" name="bidan" style="background: rgba(250, 250, 250, 0.1);">
					<!-- <option>--Pilih Nama Bidan--</option> -->
					<option value="<?php echo $dataBidan['id'] ?>">
						<?php echo $dataBidan['nama_bidan'] ?>
					</option>
				</select>
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<select class="form-control untukInput2" name="pasien" id="pasien" style="background: rgba(250, 250, 250, 0.1);">
					<option>--Pilih Nama Pasien--</option>
					<?php
						include 'koneksi.php';
						$pasien 		= "SELECT * FROM pasien";
						$queryPasien = mysqli_query($conn,$pasien);
						while ($dataPasien = mysqli_fetch_array($queryPasien)) { ?>
						    <option value="<?php echo $dataPasien['id'] ?>"><?php echo $dataPasien['nama_pasien'] ?>
						    </option>
					<?php
					}
					?>
				</select>
			</div>
			<br>
			<div class="form-group col-sm-10">
				<input type="text" class="form-control untukInput1" name="umur" id="umur" placeholder="Umur">
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<select class="form-control untukInput2" name="minggu" style="background: rgba(250, 250, 250, 0.1);">
			  		<option>Minggu ke-</option>
			  		<option value="1">1</option>
			  		<option value="2">2</option>
			  		<option value="3">3</option>
			  		<option value="4">4</option>
			  		<option value="5">5</option>
			  		<option value="6">6</option>
			  		<option value="7">7</option>
			  		<option value="8">8</option> 
			  		<option value="9">9</option>
			  		<option value="10">10</option>
			  		<option value="11">11</option>
			  		<option value="12">12</option>
			  		<option value="13">13</option>
			  		<option value="14">14</option>
			  		<option value="15">15</option>
			  		<option value="16">16</option>
			  		<option value="17">17</option>
			  		<option value="18">18</option>
			  		<option value="19">19</option>
			  		<option value="20">20</option>
			  		<option value="21">21</option>
			  		<option value="22">22</option>
			  		<option value="23">23</option>
			  		<option value="24">24</option>
			  		<option value="25">25</option>
			  		<option value="26">26</option>
			  		<option value="27">27</option>
			  		<option value="28">28</option>
			  		<option value="29">29</option>
			  		<option value="30">30</option>
			  		<option value="31">31</option>
			  		<option value="32">32</option>
			  		<option value="33">33</option>
			  		<option value="34">34</option>
			  		<option value="35">35</option>
			  		<option value="36">36</option>
			  	</select>
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<input type="text" class="form-control untukInput1" name="tekanan_darah" placeholder="Tekanan Darah (sistol/diastol)" style="background: rgba(250, 250, 250, 0.1);">
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<input type="text" class="form-control untukInput1" name="berat_badan" placeholder="Berat Badan" style="background: rgba(250, 250, 250, 0.1);">
			</div>
			<br>
			<br>
			<div class="form-group col-sm-10">
			  	<b>Riwayat Penyakit</b>
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<select class="form-control untukInput2" name="penyakit_menular" style="background: rgba(250, 250, 250, 0.1);">
			  		<option>Penyakit Menular</option>
			  		<option value="0">Tidak</option>
			  		<option value="1">Iya</option>
			  	</select>
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<select class="form-control untukInput2" name="mata_minus" style="background: rgba(250, 250, 250, 0.1);">
			  		<option>Mata Minus > 5</option>
			  		<option value="0">Tidak</option>
			  		<option value="1">Iya</option>
			  	</select>
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<select class="form-control untukInput2" name="asma" style="background: rgba(250, 250, 250, 0.1);">
			  		<option>Asma</option>
			  		<option value="0">Tidak Mengidap</option>
			  		<option value="1">Ringan Berkala</option>
			  		<option value="2">Ringan Menetap</option>
			  		<option value="3">Sedang Menetap</option>
			  		<option value="4">Parah Menetap</option>
			  	</select>
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<select class="form-control untukInput2" name="jantung" style="background: rgba(250, 250, 250, 0.1);">
			  		<option>Jantung</option>
			  		<option value="0">Tidak</option>
			  		<option value="1">Iya</option>
			  	</select>
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<select class="form-control untukInput2" name="hipertensi" style="background: rgba(250, 250, 250, 0.1);">
			  		<option>Hipertensi</option>
			  		<option value="0">Tidak Hipertensi</option>
			  		<option value="1">Hipertensi Ringan</option>
			  		<option value="2">Hipertensi Sedang</option>
			  		<option value="3">Hipertensi Berat</option>
			  	</select>
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<select class="form-control untukInput2" name="diabetes" style="background: rgba(250, 250, 250, 0.1);">
			  		<option>Diabetes</option>
			  		<option value="0">Tidak Diabetes</option>
			  		<option value="1">Diabetes Tipe 1</option>
			  		<option value="2">Diabetes Tipe 2</option>
			  	</select>
			</div>
			<br>
			<br>
			<div class="form-group col-sm-10">
			  	<b>Riwayat Kehamilan Sebelumnya</b>
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<select class="form-control untukInput2" name="sesar" style="background: rgba(250, 250, 250, 0.1);">
			  		<option>Sesar < 5 Tahun</option>
			  		<option value="0">Tidak</option>
			  		<option value="1">Iya</option>
			  	</select>
			</div>
			<br>
			<br>
			<div class="form-group col-sm-10">
			  	<b>Faktor Fisiologis Ibu</b>
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<select class="form-control untukInput2" name="pinggul" style="background: rgba(250, 250, 250, 0.1);">
			  		<option>Pinggul</option>
			  		<option value="0">Tidak</option>
			  		<option value="1">Iya</option>
			  	</select>
			</div>
			<br>
			<br>
			<div class="form-group col-sm-10">
			  	<b>Riwayat Kehamilan Saat Ini</b>
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<select class="form-control untukInput2" name="p_previa" style="background: rgba(250, 250, 250, 0.1);">
			  		<option>Plasenta Previa</option>
			  		<option value="0">Tidak</option>
			  		<option value="1">Iya</option>
			  	</select>
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<select class="form-control untukInput2" name="b_sungsang" style="background: rgba(250, 250, 250, 0.1);">
			  		<option>Bayi Sungsang</option>
			  		<option value="0">Tidak</option>
			  		<option value="1">Iya</option>
			  	</select>
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<select class="form-control untukInput2" name="b_kembar" style="background: rgba(250, 250, 250, 0.1);">
			  		<option>Bayi Kembar</option>
			  		<option value="0">Tidak</option>
			  		<option value="1">Iya</option>
			  	</select>
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<select class="form-control untukInput2" name="b_lemah" style="background: rgba(250, 250, 250, 0.1);">
			  		<option>Detak Jantung Bayi Lemah</option>
			  		<option value="0">Tidak</option>
			  		<option value="1">Iya</option>
			  	</select>
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<select class="form-control untukInput2" name="fetal_distress" style="background: rgba(250, 250, 250, 0.1);">
			  		<option>Fetal Distress</option>
			  		<option value="0">Tidak</option>
			  		<option value="1">Iya</option>
			  	</select>
			</div>
			<br>
			<div class="form-group col-sm-10">
			  	<select class="form-control untukInput2" name="b_giant" style="background: rgba(250, 250, 250, 0.1);">
			  		<option>Bayi Giant</option>
			  		<option value="0">Tidak</option>
			  		<option value="1">Iya</option>
			  	</select>
			</div>
			<br>
			<div>
				&nbsp;&nbsp;<button type="submit" class="btn btn-dark">Input</button>
			</div>
		</form>	
	</div>
</body>
</html>

<script type="text/javascript">
	$( "#pasien" ).change(function() {
	  var id = $("#pasien").val();
	  console.log(id);
	  $.ajax({
	    url: "./ajax_umur.php?id=" + id,
	    success: function(result){
	      $("#umur").val(result);
	    }
	  });
	});

	function closeNav() {
	    document.getElementById("mySidenav").style.width = "0";
	}

	function openNav() {
	    document.getElementById("mySidenav").style.width = "250px";
	    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
	}
</script>