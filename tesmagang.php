<!DOCTYPE html>
<html>
<head>
	<title></title>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	  <script src="../bootstrap/js/jquery.min.js"></script>
	  <script src="../bootstrap/js/bootstrap.min.js"></script>
	  <style type="text/css">
	    /* Remove the navbar's default margin-bottom and rounded borders */ 
	    .navbar {
	      margin-bottom: 0;
	      border-radius: 0;
	    }
	    
	    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
	    .row.content {height: 450px}
	    
	    /* Set gray background color and 100% height */
	    .sidenav {
	      padding-top: 20px;
	      background-color: #f1f1f1;
	      height: 100%;
	    }
	    
	    /* Set black background color, white text and some padding */
	    footer {
	      background-color: #555;
	      color: white;
	      padding: 15px;
	    }
	    
	    /* On small screens, set height to 'auto' for sidenav and grid */
	    @media screen and (max-width: 767px) {
	      .sidenav {
	        height: auto;
	        padding: 15px;
	      }
	      .row.content {height:auto;} 
	    }
	    .navbar-inverse {
	    background-color: #2F4F4F ;
	    font-size:18px;
	  }
	  body {
	      background-color: #3bb2b8;
	      background: linear-gradient(
	        25deg,
	        rgba(66, 230, 149, 1),
	        rgba(66, 32, 149, 1)
	      );
	      background-repeat: no-repeat;
	      height: 100vh;
	      font-family: "Open Sans", sans-serif;
	  }
	  .container {
	    max-width: 800px;
	    margin: 5% auto;
	    padding: 20px;
	    background-color: #fff;
	    border-radius: 10px;
	    overflow: hidden;
	    box-sizing: border-box;
	    box-shadow: 0 10px 35px rgba(0, 0, 0, 0.4);
	  }

	  .text-center {
	    text-align: center;
	    margin-bottom: 1em;
	  }

	  .lightbox-gallery {
	    display: flex;
	    flex-direction: row;
	    flex-wrap: wrap;
	    justify-content: center;
	  }

	  .lightbox-gallery div > img {
	    max-width: 100%;
	    display: block;
	  }

	  .lightbox-gallery div {
	    margin: 10px;
	    flex-basis: 180px;
	  }

	  @media only screen and (max-width: 480px) {
	    .lightbox-gallery {
	      flex-direction: column;
	      align-items: center;
	    }

	    .lightbox > div {
	      margin-bottom: 10px;
	    }
	  }

	  /Lighbox CSS/

	  .lightbox {
	    display: none;
	    width: 100%;
	    height: 100%;
	    background-color: rgba(0, 0, 0, 0.7);
	    position: fixed;
	    top: 0;
	    left: 0;
	    z-index: 20;
	    padding-top: 30px;
	    box-sizing: border-box;
	  }

	  .lightbox img {
	    display: block;
	    margin: auto;
	  }

	  .lightbox .caption {
	    margin: 15px auto;
	    width: 50%;
	    text-align: center;
	    font-size: 1em;
	    line-height: 1.5;
	    font-weight: 700;
	    color: #eee;
	  }

	  .github-link {
	    font-size: 18px;
	    color: rgba(255, 255, 255, 0.7);
	  }

	  .github-link:hover,
	  .github-link:active,
	  .github-link:visited {
	    color: #fff;
	    text-decoration: none;
	  }
	</style>
</head>
<body>
	  <nav class="navbar navbar-inverse">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>                        
	      </button>
	      <!-- <a class="navbar-brand" href="#"><img src="../Images/grf.png" width="80" height="35"> -->
	      <a class="navbar-brand" href="#"><img src="../Images/grf.png" style="height: 185%;">
	      <a class="navbar-brand" href="#"><img src="../Images/hmm.png" style="height: 185%;">
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href="#"><span class="glyphicon glyphicon-home"></span> Home</a></li>
	        <li><a href="#">Daftar Alat</a></li>
	      </ul>
	      <ul class="nav navbar-nav navbar-right">
	        <li></center><a href="1index.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
	      </ul>
	    </div>
	  </div>
	</nav>
	  <div class="container">
	    <h2 class="text-center">Daftar Alat</h2>
	    <div class="lightbox-gallery">
	        <div><img src="../../Images/1.png" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit, quae, quam. Ut dolorum quia, unde dicta at harum porro officia obcaecati ipsam deserunt fugit dolore delectus quam, maxime nisi quo."></div>

	        <div><img src="../../Images/2.png" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime accusamus officiis dignissimos doloribus consectetur harum eos sapiente optio aut minima."></div>

	        <div><img src="../../Images/5.png" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates veritatis similique, amet, maiores soluta recusandae cupiditate, sed perspiciatis fugit minima, sunt dolores cum earum deserunt illo ipsum!"></div>

	        <div><img src="../../Images/6.png" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque laudantium dignissimos tenetur eos unde quidem repellat officiis nemo laboriosam necessitatibus deleniti commodi quis aliquid est atque tempora aut, nihil!"></div>

	        <div><img src="../../Images/7.png" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto minus consequatur soluta quaerat itaque, laboriosam quis a facilis, cumque, deleniti quas aperiam voluptate dolore. Enim nostrum sit eaque, porro eligendi illo placeat?"></div>

	        <div><img src="../../Images/8.png" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi suscipit quam, id aliquam totam aperiam quas rem debitis voluptatem pariatur, illo accusamus facilis eius ipsa! Reprehenderit libero, quas iste repudiandae distinctio, quos dignissimos."></div>

	        <div><img src="../../Images/9.png" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi suscipit quam, id aliquam totam aperiam quas rem debitis voluptatem pariatur, illo accusamus facilis eius ipsa! Reprehenderit libero, quas iste repudiandae distinctio, quos dignissimos."></div>

	        <div><img src="../../Images/10.png" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi suscipit quam, id aliquam totam aperiam quas rem debitis voluptatem pariatur, illo accusamus facilis eius ipsa! Reprehenderit libero, quas iste repudiandae distinctio, quos dignissimos."></div>

	        <div><img src="../../Images/11.png" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi suscipit quam, id aliquam totam aperiam quas rem debitis voluptatem pariatur, illo accusamus facilis eius ipsa! Reprehenderit libero, quas iste repudiandae distinctio, quos dignissimos."></div>

	        <div><img src="../../Images/12.png" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi suscipit quam, id aliquam totam aperiam quas rem debitis voluptatem pariatur, illo accusamus facilis eius ipsa! Reprehenderit libero, quas iste repudiandae distinctio, quos dignissimos."></div>

	        <div><img src="../../Images/13.png" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi suscipit quam, id aliquam totam aperiam quas rem debitis voluptatem pariatur, illo accusamus facilis eius ipsa! Reprehenderit libero, quas iste repudiandae distinctio, quos dignissimos."></div>
	    </div>
	  </div>
	<div class="text-center github-link">
	  
	  <a class="github-link" href="https://github.com/humbl3man/lightboxify" target="_blank"><i class="fa fa-github"></i> Github Source Code</a>
	  
	</div>4
	<footer class="container-fluid text-center">
	  <p>"Bermimpilah setinggi langit dan jangan pernah takut jatuh,karena ketika kalian jatuh masih banyak bintang yang mau menopang mimpi-mimpi kalian" -Pak Hamrowi</p>
	  <p>"Seni itu Indah dan kamu adalah seni" -Pak Polo</p>
	</footer>
</body>
</html>
<script type="text/javascript">
// Create a lightbox
(function() {
  var $lightbox = $("<div class='lightbox'></div>");
  var $img = $("<img>");
  var $caption = $("<p cla ss='caption'></p>");

  // Add image and caption to lightbox

  $lightbox
    .append($img)
    .append($caption);

  // Add lighbox to document

  $('body').append($lightbox);

  $('.lightbox-gallery img').click(function(e) {
    e.preventDefault();

    // Get image link and description
    var src = $(this).attr("data-image-hd");
    var cap = $(this).attr("alt");

    // Add data to lighbox

    $img.attr('src', src);
    $caption.text(cap);

    // Show lightbox

    $lightbox.fadeIn('fast');

    $lightbox.click(function() {
      $lightbox.fadeOut('fast');
    });
  });

}());
</script>