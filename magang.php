<?php
	
	session_start();

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<style type="text/css">
		body {
		  background-color: #3bb2b8;
		  background: linear-gradient(
		    25deg,
		    rgba(66, 230, 149, 1),
		    rgba(66, 32, 149, 1)
		  );
		  background-repeat: no-repeat;
		  height: 100vh;
		  font-family: "Open Sans", sans-serif;
		}
		.container {
		  max-width: 800px;
		  margin: 5% auto;
		  padding: 20px;
		  background-color: #fff;
		  border-radius: 10px;
		  overflow: hidden;
		  box-sizing: border-box;
		  box-shadow: 0 10px 35px rgba(0, 0, 0, 0.4);
		}

		.text-center {
		  text-align: center;
		  margin-bottom: 1em;
		}

		.lightbox-gallery {
		  display: flex;
		  flex-direction: row;
		  flex-wrap: wrap;
		  justify-content: center;
		}

		.lightbox-gallery div > img {
		  max-width: 100%;
		  display: block;
		}

		.lightbox-gallery div {
		  margin: 10px;
		  flex-basis: 180px;
		}

		@media only screen and (max-width: 480px) {
		  .lightbox-gallery {
		    flex-direction: column;
		    align-items: center;
		  }

		  .lightbox > div {
		    margin-bottom: 10px;
		  }
		}

		/Lighbox CSS/

		.lightbox {
		  display: none;
		  width: 100%;
		  height: 100%;
		  background-color: rgba(0, 0, 0, 0.7);
		  position: fixed;
		  top: 0;
		  left: 0;
		  z-index: 20;
		  padding-top: 30px;
		  box-sizing: border-box;
		}

		.lightbox img {
		  display: block;
		  margin: auto;
		}

		.lightbox .caption {
		  margin: 15px auto;
		  width: 50%;
		  text-align: center;
		  font-size: 1em;
		  line-height: 1.5;
		  font-weight: 700;
		  color: #eee;
		}

		.github-link {
		  font-size: 18px;
		  color: rgba(255, 255, 255, 0.7);
		}

		.github-link:hover,
		.github-link:active,
		.github-link:visited {
		  color: #fff;
		  text-decoration: none;
		}
	</style>
</head>
<body>
	<header>
	  <input type="checkbox" id="tag-menu"/>
	  <label class="fa fa-bars menu-bar" for="tag-menu"></label><font style="color: white">&nbsp;&nbsp;Beranda Pasien</font>
	  <div class="jw-drawer">
	    <nav>
<!-- 	    	<ul>
	    		<br>
	    		<br>
	    		<br>
	    		<br>
	    		<br>

	    	</ul>
			<ul style="background-color: white">
				<li>
					<a href="datadiribidan.php"><i class="fa fa-file"></i>&nbsp;&nbsp;<font size="2">Data Diri</font></a>
				</li>
				<li>
					<a href="inputpasien.php"><i class="fa fa-plus"></i>&nbsp;&nbsp;<font size="2">Input Pasien</font></a>
				</li>
				<li>
					<a href="inputcheckup.php"><i class="fa fa-plus"></i>&nbsp;&nbsp;<font size="2">Input Hasil Checkup</font></a>
				</li>
				<li>
					<a href="logout.php"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;<font size="2">Logout</font></a>
				</li>
		        <br>
		        <br>
		        <br>
		        <br>
		        <br>
		        <br>
			</ul> -->
	    </nav>
	  </div>
	</header>

	<div class="container">
		<br>
		<div class="container">
		<h2 class="text-center">Lightbox Gallery</h2>
		<div class="lightbox-gallery">
				<div><img src="http://placehold.it/300/f1b702/fff&text=image1" data-image-hd="http://placehold.it/600/f1b702/fff&text=image1" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit, quae, quam. Ut dolorum quia, unde dicta at harum porro officia obcaecati ipsam deserunt fugit dolore delectus quam, maxime nisi quo."></div>
				<div><img src="http://placehold.it/300/d2f1b2/222&text=image2" data-image-hd="http://placehold.it/600/d2f1b2/222&text=image2" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime accusamus officiis dignissimos doloribus consectetur harum eos sapiente optio aut minima."></div>
				<div><img src="http://placehold.it/300/eee/000&text=image3" data-image-hd="http://placehold.it/600/eee/000&text=image3" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates veritatis similique, amet, maiores soluta recusandae cupiditate, sed perspiciatis fugit minima, sunt dolores cum earum deserunt illo ipsum!"></div>
				<div><img src="http://placehold.it/300/222/fff&text=image4" data-image-hd="http://placehold.it/600/222/fff&text=image4" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque laudantium dignissimos tenetur eos unde quidem repellat officiis nemo laboriosam necessitatibus deleniti commodi quis aliquid est atque tempora aut, nihil!"></div>
				<div><img src="http://placehold.it/300/b47f99/000&text=image5" data-image-hd="http://placehold.it/600/b47f99/000&text=image5" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto minus consequatur soluta quaerat itaque, laboriosam quis a facilis, cumque, deleniti quas aperiam voluptate dolore. Enim nostrum sit eaque, porro eligendi illo placeat?"></div>
				<div><img src="http://placehold.it/300/e1d400/000&text=image6" data-image-hd="http://placehold.it/600/e1d400/000&text=image6" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi suscipit quam, id aliquam totam aperiam quas rem debitis voluptatem pariatur, illo accusamus facilis eius ipsa! Reprehenderit libero, quas iste repudiandae distinctio, quos dignissimos."></div>
		</div>
	</div>


<div class="text-center github-link">
  
  <a class="github-link" href="https://github.com/humbl3man/lightboxify" target="_blank"><i class="fa fa-github"></i> Github Source Code</a>
  
</div>
	</div>
</body>
</html>
<script type="text/javascript">
	// Create a lightbox
	(function() {
	  var $lightbox = $("<div class='lightbox'></div>");
	  var $img = $("<img>");
	  var $caption = $("<p class='caption'></p>");

	  // Add image and caption to lightbox

	  $lightbox
	    .append($img)
	    .append($caption);

	  // Add lighbox to document

	  $('body').append($lightbox);

	  $('.lightbox-gallery img').click(function(e) {
	    e.preventDefault();

	    // Get image link and description
	    var src = $(this).attr("data-image-hd");
	    var cap = $(this).attr("alt");

	    // Add data to lighbox

	    $img.attr('src', src);
	    $caption.text(cap);

	    // Show lightbox

	    $lightbox.fadeIn('fast');

	    $lightbox.click(function() {
	      $lightbox.fadeOut('fast');
	    });
	  });

	}());
</script>