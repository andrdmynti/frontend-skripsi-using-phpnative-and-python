<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<style type="text/css">
		* {
			padding:0;
			margin:0;
		}

		body {
			font-family:Verdana, Geneva, sans-serif;
			font-size:18px;
			background-color:#FFF
		}

		header {
			width:100%;
			background-color:#006faa ;
			z-index:1000;
		}

		.menu-bar {
			color:#FFF;
			font-size:25px;
			cursor:pointer;
			padding:10px 12px;
			margin-left:10px;
			margin-top:5px;
			margin-bottom:5px;
		}

		.menu-bar:hover {
			background-color:rgba(0, 0, 0, 0.1);
			border-radius:50px;
		}

		#tag-menu {
			display:none;
		}


		#tag-menu:checked ~ div.jw-drawer {
		 animation: slide-in 0.5s ease;
		 animation-fill-mode: forwards;
		}

		.jw-drawer {
			position:fixed;
			left:-280px;
			background-color:#006faa;
			height:100%;
			z-index:100;
			width:230px;
			animation: slide-out 0.5s ease;
			animation-fill-mode: forwards;
		}

		.jw-drawer ul li {
			list-style:none;
		}

		.jw-drawer ul li a {
			padding:10px 20px;
			text-decoration:none;
			display:block;
			color:#FFF;
			border-top:1px solid #059;
		}

		.jw-drawer ul li a:hover{
			background-color:rgba(0, 0, 0, 0.1);
		}

		.jw-drawer ul li a i {
			width:50px;
			height:35px;
			text-align:center;
			padding-top:15px;
		}

		@keyframes slide-in {
		 from {left: -280px;}
		 to {left: 0;}
		}

		@keyframes slide-out {
		 from {left: 0;}
		 to {left: -280px;}
		}
	</style>
</head>
<body>
	<header>
	  <input type="checkbox" id="tag-menu"/>
	  <a href="artikel.php"><label class="fa fa-arrow-left menu-bar"></label></a><font color="white">Detail Artikel</font>
	  <div class="jw-drawer">
	    <nav>
	      <ul>
	       <li>
	        	<a href="datadiripasien.php"><i class="fa fa-user"></i>&nbsp;&nbsp;<font size="2">Data Diri</font></a>
	        </li>
	        <li>
	        	<a href="hasilcheckup.php"><i class="fa fa-file"></i>&nbsp;&nbsp;<font size="2">Hasil Checkup</font></a>
	        </li>
	        <li>
	        	<a href="artikel.php"><i class="fa fa-newspaper-o"></i>&nbsp;&nbsp;<font size="2">Artikel</font></a>
	        </li>
	        <li>
	        	<a href="logout.php"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;<font size="2">Logout</font></a>
	        </li>
	      </ul>
	    </nav>
	  </div>
	</header>

	<div class="container">
		<br>
		<center>
			<?php

				include 'koneksi.php';

				$id 		= $_GET['id'];

				$detail    	= "SELECT * FROM contents WHERE id = '$id'";
				$hasil   	= mysqli_query($conn, $detail)or die(mysqli_error());
				$data    	= mysqli_fetch_array($hasil);	


				$query = mysqli_query($conn, "SELECT * FROM contents WHERE id=$id")or die(mysqli_error());
					if(mysqli_num_rows($query) == 0){	
						echo '<tr><td colspan="5" align="center">Tidak ada artikel!</td></tr>';	
					}
						else
					{	
						while($data = mysqli_fetch_array($query)){
						?>
								<div class="card bg-light text-dark">
									<img class="card-img-top" src="<?php echo $data['picture'] ?>" alt="<?php echo $data['title'] ?>" style="width:100%">
								   	<div class="card-body">
								    	<h4 class="card-title"><?php echo $data['title'] ?></h4>
								     	<p><?php echo $data['news'] ?></p>
								     	<p><?php echo $data['created_at'] ?></p>
								   </div>
								</div>
							
							<br>
							<br>
						<?php
						}
					}
			?>


		</center>
	</div>
</body>
</html>