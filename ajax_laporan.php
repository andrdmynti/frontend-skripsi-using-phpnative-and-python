<?php

	include "koneksi.php";

	$id 		 = $_GET["id"];
	$query 		 = "SELECT * FROM rekamedis WHERE pasien_id = '$id'";
	$sql 		 = mysqli_query($conn,$query)or die(mysqli_error($conn));
	while ($row  = mysqli_fetch_array($sql)) {

	// while ($dataPasien = mysqli_fetch_array($queryPasien)) {
	$sistol 	 	= $row['sistol'];
	$diastol 	 	= $row['diastol'];
	$tekanan_darah 	= $sistol.'/'.$diastol;

?>
<div>
	<tr>
		<td>Minggu Ke-</td>
		<td>:</td>
		<td>
			<?php 
				echo $row['minggu_ke'];
			?>
		</td>
	</tr>
	<br>
	<tr>
		<td>Tekanan Darah</td>
		<td>:</td>
		<td>
			<?php 
				echo $tekanan_darah;
			?>
		</td>
	</tr>
	<br>
	<tr>
		<td>Penyakit Menular</td>
		<td>:</td>
		<td>
			<?php 
				if($row['penyakit_menular']=='0') {
					echo 'Tidak';
				}
				else {
					echo 'Iya';
				}
			?>
		</td>
	</tr>
	<br>
	<tr>
		<td>Mata Minus</td>
		<td>:</td>
		<td>
			<?php 
				if($row['mata_minus']=='0') {
					echo 'Tidak';
				}
				else {
					echo 'Iya';
				}
			?>
		</td>
	</tr>
	<br>
	<tr>
		<td>Asma</td>
		<td>:</td>
		<td>
			<?php 
				if($row['asma']=='0') {
					echo 'Tidak Mengidap Asma';
				}
				if($row['asma']=='1') {
					echo 'Asma Ringan Berkala';
				}
				elseif($row['asma']=='2'){
					echo 'Asma Ringan Menetap';
				}
				elseif($row['asma']=='3'){
					echo 'Asma Sedang Menetap';
				}
				else {
					echo 'Asma Sedang Menetap';
				}
			?>
		</td>
	</tr>
	<br>
	<tr>
		<td>Jantung</td>
		<td>:</td>
		<td>
			<?php 
				if($row['jantung']=='0') {
					echo 'Tidak';
				}
				else {
					echo 'Iya';
				}
			?>
		</td>
	</tr>
	<br>
	<tr>
		<td>Hipertensi</td>
		<td>:</td>
		<td>
			<?php 
				if($row['hipertensi']=='0') {
					echo 'Tidak Hipertensi';
				}
				elseif($row['hipertensi']=='1') {
					echo 'Hipertensi Ringan';
				}
				elseif($row['hipertensi']=='2'){
					echo 'Hipertensi Sedang';
				}
				else {
					echo 'Hipertensi Berat';
				}
			?>
		</td>
	</tr>
	<br>
	<tr>
		<td>Diabetes</td>
		<td>:</td>
		<td>
			<?php 
				if($row['diabetes']=='0') {
					echo 'Tidak Diabetes';
				}
				elseif($row['diabetes']=='1') {
					echo 'Diabetes Tipe 1';
				}
				else {
					echo 'Diabetes Tipe 2';
				}
			?>
		</td>
	</tr>
	<br>
	<tr>
		<td>Sesar < 5 tahun</td>
		<td>:</td>
		<td>
			<?php 
				if($row['sesar']=='0') {
					echo 'Tidak';
				}
				else {
					echo 'Iya';
				}
			?>
		</td>
	</tr>
	<br>
	<tr>
		<td>Pinggul Kecil</td>
		<td>:</td>
		<td>
			<?php 
				if($row['pinggul']=='0') {
					echo 'Tidak';
				}
				else {
					echo 'Iya';
				}
			?>
		</td>
	</tr>
	<br>
	<tr>
		<td>Plasenta Previa</td>
		<td>:</td>
		<td>
			<?php 
				if($row['p_previa']=='0') {
					echo 'Tidak';
				}
				else {
					echo 'Iya';
				}
			?>
		</td>
	</tr>
	<br>
	<tr>
		<td>Posisi Bayi Sungsang</td>
		<td>:</td>
		<td>
			<?php 
				if($row['b_sungsang']=='0') {
					echo 'Tidak';
				}
				else {
					echo 'Iya';
				}
			?>
		</td>
	</tr>
	<br>
	<tr>
		<td>Bayi Kembar</td>
		<td>:</td>
		<td>
			<?php 
				if($row['b_kembar']=='0') {
					echo 'Tidak';
				}
				else {
					echo 'Iya';
				}
			?>
		</td>
	</tr>
	<br>
	<tr>
		<td>Jantung Bayi Lemah</td>
		<td>:</td>
		<td>
			<?php 
				if($row['b_jantung_lemah']=='0') {
					echo 'Tidak';
				}
				else {
					echo 'Iya';
				}
			?>
		</td>
	</tr>
	<br>
	<tr>
		<td>Fetal Distress</td>
		<td>:</td>
		<td>
			<?php 
				if($row['fetal_distress']=='0') {
					echo 'Tidak';
				}
				else {
					echo 'Iya';
				}
			?>
		</td>
	</tr>
	<br>
	<tr>
		<td>Giant Baby</td>
		<td>:</td>
		<td>
			<?php 
				if($row['b_giant']=='0') {
					echo 'Tidak';
				}
				else {
					echo 'Iya';
				}
			?>
		</td>
	</tr>
	<br>
	<hr>
	<br>
</div>
<?php

}
?>