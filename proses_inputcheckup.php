<?php

	include 'koneksi.php';

	$bidan		   		= $_POST["bidan"];
	$pasien		   		= $_POST["pasien"];
	$umur		   		= $_POST["umur"];
	$minggu		   		= $_POST["minggu"];
	$tekanan_darah		= $_POST["tekanan_darah"];
	$replace 			= str_replace('/', '', $tekanan_darah);
	$sistol			 	= substr($replace, 0,3);
	$diastol			= substr($replace, 3,5);
	$penyakit_menular	= $_POST["penyakit_menular"];
	$mata_minus			= $_POST["mata_minus"];
	$asma				= $_POST["asma"];
	$jantung			= $_POST["jantung"];
	$hipertensi			= $_POST["hipertensi"];
	$diabetes			= $_POST["diabetes"];
	$sesar				= $_POST["sesar"];
	$pinggul			= $_POST["pinggul"];
	$p_previa			= $_POST["p_previa"];
	$b_sungsang			= $_POST["b_sungsang"];
	$b_kembar			= $_POST["b_kembar"];
	$b_lemah			= $_POST["b_lemah"];
	$fetal_distress		= $_POST["fetal_distress"];
	$b_giant			= $_POST["b_giant"];
	$created 			= date('Y-m-d h:m:s');

	$url = 'http://0.0.0.0:5000/api/v1.0/predict';
	$data = [
		'umur' => $umur,
		// 'sistol' => $sistol, 
		// 'diastol' => $diastol,
		'penyakit_menular' => $penyakit_menular,
		'mata_minus' => $mata_minus,
		'asma' => $asma,
		'jantung' => $jantung,
		'hipertensi' => $hipertensi,
		'diabetes' => $diabetes,
		'sesar' => $sesar,
		'pinggul' => $pinggul,	
		'plasenta_previa' => $p_previa,
		'sungsang' => $b_sungsang,
		'kembar' => $b_kembar,
		'jantung_bayi_lemah' => $b_lemah,
		'fetal_distress' => $fetal_distress,
		'giant_baby' => $b_giant
	];

	// // use key 'http' even if you send the request to https://...
	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($data)
		)
	);
	
	$context  	= stream_context_create($options);
	$result 	= file_get_contents($url, false, $context);
	if ($result === FALSE) { /* Handle error */ }
	$result = json_decode($result);

	// var_dump($result);
    $hasil  		   = $result->response->sesar;
    $percentage_normal = $result->response->percentage_normal;
    $percentage_sesar  = $result->response->percentage_sesar;

    // var_dump($hasil);
    // var_dump($percentage_normal);
    // var_dump($percentage_sesar);
    
    $insert			= "INSERT INTO rekamedis (bidan_id,pasien_id,sistol,diastol,minggu_ke, penyakit_menular, mata_minus, asma, jantung, hipertensi, diabetes, sesar, pinggul, p_previa, b_sungsang, b_kembar, b_jantung_lemah, fetal_distress, b_giant, percentage_sesar, percentage_normal, hasil, created_at) VALUES ('$bidan','$pasien','$sistol','$diastol', '$minggu', '$penyakit_menular', '$mata_minus', '$asma', '$jantung','$hipertensi','$diabetes','$sesar','$pinggul','$p_previa','$b_sungsang','$b_kembar','$b_lemah','$fetal_distress', '$b_giant', '$percentage_sesar', '$percentage_normal', '$hasil', '$created')";

	$simpan			= mysqli_query($conn, $insert)or die(mysqli_error($conn));
	echo '<META HTTP-EQUIV="REFRESH" CONTENT = "1; URL=lihat_hasilpredict.php">';  

?>