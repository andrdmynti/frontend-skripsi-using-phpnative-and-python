import pandas as pd
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier

# Data preprocessing
# Drop everything we don't need
# Change our label y to one-hot encoding
# Split our data into two parts, train and testing
andrea_df = pd.read_excel("data/data.xlsx", "Sheet1", index_col=None, na_values=["NA"])
andrea_df.index = andrea_df["umur"]
andrea_df.index = andrea_df["penyakit_menular_seksual"]
andrea_df.index = andrea_df["mata_minus_dibawah_5"]
andrea_df.index = andrea_df["asma"]
andrea_df.index = andrea_df["jantung"]
andrea_df.index = andrea_df["hipertensi"]
andrea_df.index = andrea_df["diabetes"]
andrea_df.index = andrea_df["faktor_fisiologis_ibu"]
andrea_df.index = andrea_df["operasi_dibawah_5"]
andrea_df.index = andrea_df["plasenta_previa"]
andrea_df.index = andrea_df["sungsang"]
andrea_df.index = andrea_df["kembar"]
andrea_df.index = andrea_df["jantung_bayi_lemah"]
andrea_df.index = andrea_df["fetal_distress"]
andrea_df.index = andrea_df["giant_baby"]
andrea_df.index = andrea_df["hasil"]
andrea_df = andrea_df.dropna()

def preprocess_df(df):
    processed_df = df.copy()
    processed_X = processed_df.drop(["hasil"], axis=1).as_matrix()
    processed_df['gagal'] = processed_df['hasil'].apply(lambda s: 1 - s)
    processed_y = processed_df[['gagal', 'hasil']].as_matrix()
    return processed_X, processed_y

dataset_X, dataset_y = preprocess_df(andrea_df)

X_train, X_test, y_train, y_test = train_test_split(dataset_X, dataset_y, test_size=0.2, random_state=42)

clf = MLPClassifier(solver='lbfgs', alpha=1e-5,
                      hidden_layer_sizes=(16, 2), random_state=1)

clf.fit(X_train, y_train)    
predicted = clf.predict(X_test)

print("accuracy : ", metrics.accuracy_score(y_test, predicted))