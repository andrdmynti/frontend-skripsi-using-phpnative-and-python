from flask import Flask, jsonify, abort, request, make_response, url_for, render_template
from flask_cors import CORS, cross_origin
import numpy as np
import pickle
import pandas as pd
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier


app = Flask(__name__, static_url_path = "/static")
cors = CORS(app)

@app.errorhandler(400)
def not_found(error):
    return make_response(jsonify( { 'error': 'Bad request' } ), 400)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify( { 'error': 'Not found' } ), 404)

def preprocess_df(df):
    processed_df = df.copy()
    processed_X = processed_df.drop(["hasil"], axis=1).as_matrix()
    processed_df['gagal'] = processed_df['hasil'].apply(lambda s: 1 - s)
    processed_y = processed_df[['gagal', 'hasil']].as_matrix()
    return processed_X, processed_y

# main route
# render index.html
@app.route('/', methods = ['GET'])
def index():
    return render_template('index.html')

# endpoint to predict the probability
# we restore our tensorflow model in model folder
# and use that to make a prediction
@app.route('/api/v1.0/predict', methods = ['POST'])
def predict():

    # Data preprocessing
    # Drop everything we don't need
    # Change our label y to one-hot encoding
    # Split our data into two parts, train and testing
    andrea_df = pd.read_excel("data/data.xlsx", "Sheet1", index_col=None, na_values=["NA"])
    andrea_df.index = andrea_df["umur"]
    andrea_df.index = andrea_df["penyakit_menular_seksual"]
    andrea_df.index = andrea_df["mata_minus_dibawah_5"]
    andrea_df.index = andrea_df["asma"]
    andrea_df.index = andrea_df["jantung"]
    andrea_df.index = andrea_df["hipertensi"]
    andrea_df.index = andrea_df["diabetes"]
    andrea_df.index = andrea_df["faktor_fisiologis_ibu"]
    andrea_df.index = andrea_df["operasi_dibawah_5"]
    andrea_df.index = andrea_df["plasenta_previa"]
    andrea_df.index = andrea_df["sungsang"]
    andrea_df.index = andrea_df["kembar"]
    andrea_df.index = andrea_df["jantung_bayi_lemah"]
    andrea_df.index = andrea_df["fetal_distress"]
    andrea_df.index = andrea_df["giant_baby"]
    andrea_df.index = andrea_df["hasil"]
    andrea_df = andrea_df.dropna()


    dataset_X, dataset_y = preprocess_df(andrea_df)

    X_train, X_test, y_train, y_test = train_test_split(dataset_X, dataset_y, test_size=0.2, random_state=42)

    clf = MLPClassifier(solver='lbfgs', alpha=1e-5,
                        hidden_layer_sizes=(16, 2), random_state=1)


    clf.fit(X_train, y_train)     
    X_predict = np.float32([[
        request.form['umur'], 
        request.form['penyakit_menular'], 
        request.form['mata_minus'], 
        request.form['asma'],
        request.form['jantung'],
        request.form['hipertensi'],
        request.form['diabetes'],
        request.form['sesar'],
        request.form['pinggul'],
        request.form['plasenta_previa'],
        request.form['sungsang'],
        request.form['kembar'],
        request.form['jantung_bayi_lemah'],
        request.form['fetal_distress'],
        request.form['giant_baby']
    ]])     

    predicted = clf.predict(X_predict)
    proba = clf.predict_proba(X_predict)
   

    response = {
        'endpoint': 'api/v1.0/predict',
        'method': 'POST',
        'percentage_sesar': round(proba[0][1],4),
        'percentage_normal': round(proba[0][0],4),
        'sesar': int(predicted[0][1])
    }

    return jsonify( { 'response': response } )

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug = True)