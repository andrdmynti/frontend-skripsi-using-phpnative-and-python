<?php
	
	session_start();

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<style type="text/css">
		* {
			padding:0;
			margin:0;
		}

		body {
			font-family:Verdana, Geneva, sans-serif;
			font-size:18px;
			background-color:#FFF
		}

		header {
			width:100%;
			background-color:#006faa ;
			z-index:1000;
		}

		.menu-bar {
			color:#FFF;
			font-size:25px;
			cursor:pointer;
			padding:10px 12px;
			margin-left:10px;
			margin-top:5px;
			margin-bottom:5px;
		}

		.menu-bar:hover {
			background-color:rgba(0, 0, 0, 0.1);
			border-radius:50px;
		}

		#tag-menu {
			display:none;
		}


		#tag-menu:checked ~ div.jw-drawer {
		 animation: slide-in 0.5s ease;
		 animation-fill-mode: forwards;
		}

		.jw-drawer {
			position:fixed;
			left:-280px;
			background-color:#006faa;
			height:100%;
			z-index:100;
			width:230px;
			animation: slide-out 0.5s ease;
			animation-fill-mode: forwards;
		}

		.jw-drawer ul li {
			list-style:none;
		}

		.jw-drawer ul li a {
			padding:10px 20px;
			text-decoration:none;
			display:block;
			color:#595a5b;
			/*border-top:1px solid #059;*/
		}

		.jw-drawer ul li a:hover{
			background-color:rgba(0, 0, 0, 0.1);
		}

		.jw-drawer ul li a i {
			width:50px;
			height:35px;
			text-align:center;
			padding-top:15px;
		}

		@keyframes slide-in {
		 from {left: -280px;}
		 to {left: 0;}
		}

		@keyframes slide-out {
		 from {left: 0;}
		 to {left: -280px;}
		}
	</style>
</head>
<body>
	<header>
	  <input type="checkbox" id="tag-menu"/>
	  <label class="fa fa-bars menu-bar" for="tag-menu"></label><font style="color: white">&nbsp;&nbsp;Beranda Pasien</font>
	  <div class="jw-drawer">
	    <nav>
	    	<ul>
	    		<br>
	    		<br>
	    		<br>
	    		<br>
	    		<br>
	    		<p style="color:white">
	    			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $_SESSION['name']; ?>
	    			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $_SESSION['email']; ?>
	    		</p>
	    	</ul>
			<ul style="background-color: white">
				<li>
					<a href="datadiribidan.php"><i class="fa fa-file"></i>&nbsp;&nbsp;<font size="2">Data Diri</font></a>
				</li>
				<li>
					<a href="inputpasien.php"><i class="fa fa-plus"></i>&nbsp;&nbsp;<font size="2">Input Pasien</font></a>
				</li>
				<li>
					<a href="inputcheckup.php"><i class="fa fa-plus"></i>&nbsp;&nbsp;<font size="2">Input Hasil Checkup</font></a>
				</li>
				<li>
					<a href="logout.php"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;<font size="2">Logout</font></a>
				</li>
		        <br>
		        <br>
		        <br>
		        <br>
		        <br>
		        <br>
			</ul>
	    </nav>
	  </div>
	</header>

	<div class="container">
		<br>
		<center>
			
		</center>
	</div>
</body>
</html>