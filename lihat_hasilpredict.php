<?php
	
	session_start();

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<style type="text/css">
		* {
			padding:0;
			margin:0;
		}

		body {
			font-family:Verdana, Geneva, sans-serif;
			font-size:18px;
			background-color:#FFF
		}

		header {
			width:100%;
			background-color:#006faa ;
			z-index:1000;
		}

		.menu-bar {
			color:#FFF;
			font-size:25px;
			cursor:pointer;
			padding:10px 12px;
			margin-left:10px;
			margin-top:5px;
			margin-bottom:5px;
		}

		.menu-bar:hover {
			background-color:rgba(0, 0, 0, 0.1);
			border-radius:50px;
		}

		#tag-menu {
			display:none;
		}


		#tag-menu:checked ~ div.jw-drawer {
		 animation: slide-in 0.5s ease;
		 animation-fill-mode: forwards;
		}

		.jw-drawer {
			position:fixed;
			left:-280px;
			background-color:#006faa;
			height:100%;
			z-index:100;
			width:230px;
			animation: slide-out 0.5s ease;
			animation-fill-mode: forwards;
		}

		.jw-drawer ul li {
			list-style:none;
		}

		.jw-drawer ul li a {
			padding:10px 20px;
			text-decoration:none;
			display:block;
			color:#FFF;
			border-top:1px solid #059;
		}

		.jw-drawer ul li a:hover{
			background-color:rgba(0, 0, 0, 0.1);
		}

		.jw-drawer ul li a i {
			width:50px;
			height:35px;
			text-align:center;
			padding-top:15px;
		}

		@keyframes slide-in {
		 from {left: -280px;}
		 to {left: 0;}
		}

		@keyframes slide-out {
		 from {left: 0;}
		 to {left: -280px;}
		}
	</style>
</head>
<body>
	<header>
	  <input type="checkbox" id="tag-menu"/>
	  <a href="inputcheckup.php"><label class="fa fa-arrow-left menu-bar"></label></a><font color="white">Detail Hasil Checkup Pasien</font>
	  <div class="jw-drawer">
	    <nav>
	      <ul>
	       <li>
	        	<a href="datadiripasien.php"><i class="fa fa-user"></i>&nbsp;&nbsp;<font size="2">Data Diri</font></a>
	        </li>
	        <li>
	        	<a href="hasilcheckup.php"><i class="fa fa-file"></i>&nbsp;&nbsp;<font size="2">Hasil Checkup</font></a>
	        </li>
	        <li>
	        	<a href="artikel.php"><i class="fa fa-newspaper-o"></i>&nbsp;&nbsp;<font size="2">Artikel</font></a>
	        </li>
	        <li>
	        	<a href="logout.php"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;<font size="2">Logout</font></a>
	        </li>
	      </ul>
	    </nav>
	  </div>
	</header>

	<div class="container">
		<br>
		<?php

			include 'koneksi.php';
		  	$querydata = mysqli_query($conn, "SELECT rekamedis.pasien_id, rekamedis.bidan_id, rekamedis.sistol, rekamedis.diastol, rekamedis.minggu_ke, rekamedis.penyakit_menular, rekamedis.mata_minus, rekamedis.asma, rekamedis.jantung, rekamedis.hipertensi, rekamedis.diabetes, rekamedis.sesar, rekamedis.pinggul, rekamedis.p_previa, rekamedis.b_sungsang, rekamedis.b_kembar, rekamedis.b_jantung_lemah, rekamedis.fetal_distress, rekamedis.b_giant, rekamedis.percentage_sesar, rekamedis.percentage_normal, rekamedis.hasil, rekamedis.created_at, bidan.id, bidan.nama_bidan, pasien.id, pasien.nama_pasien FROM rekamedis, bidan, pasien WHERE rekamedis.pasien_id = pasien.id AND rekamedis.bidan_id = bidan.id ORDER BY rekamedis.id DESC LIMIT 1")or die(mysqli_error($conn));
                    if(mysqli_num_rows($querydata) == 0){
                      echo '<table><tr><td colspan="5" align="center">Tidak ada data!</td></tr></table>';
                    }
                      else
                    {
                  
                      while($data = mysqli_fetch_array($querydata)){
                        echo '<table>';
                        echo '<tr><td width="200">Nama Bidan</td><td width="20">:</td><td>'.$data['nama_bidan'].'</td></tr>';
                        echo '<tr><td width="200">Nama Pasien</td><td width="20">:</td><td>'.$data['nama_pasien'].'</td></tr>';
                        echo '<tr><td width="200">Minggu Ke-</td><td width="20">:</td><td>'.$data['minggu_ke'].'</td></tr>';
                        echo '<tr><td width="200">Tekanan Darah</td><td>:</td><td>'.$data['sistol'].'/'.$data['diastol'].'</td></tr>';
                        ?>
                        <tr>
                        	<td>Penyakit Menular</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['penyakit_menular']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Mata Minus</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['mata_minus']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Asma</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['asma']=='0') {
                        				echo 'Tidak Mengidap Asma';
                        			}
                        			elseif($data['asma']=='1') {
                        				echo 'Asma Ringan Berkala';
                        			}
                        			elseif($data['asma']=='2'){
                        				echo 'Asma Ringan Menetap';
                        			}
                        			elseif($data['asma']=='3'){
                        				echo 'Asma Sedang Menetap';
                        			}
                        			else {
                        				echo 'Asma Sedang Menetap';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Jantung</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['jantung']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Hipertensi</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['hipertensi']=='0') {
                        				echo 'Tidak Hipertensi';
                        			}
                        			elseif($data['hipertensi']=='1') {
                        				echo 'Hipertensi Ringan';
                        			}
                        			elseif($data['hipertensi']=='2'){
                        				echo 'Hipertensi Sedang';
                        			}
                        			else {
                        				echo 'Hipertensi Berat';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Diabetes</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['diabetes']=='0') {
                        				echo 'Tidak Diabetes';
                        			}
                        			elseif($data['diabetes']=='1') {
                        				echo 'Diabetes Tipe 1';
                        			}
                        			else {
                        				echo 'Diabetes Tipe 2';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Sesar</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['sesar']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Pinggul Kecil</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['pinggul']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Plasenta Previa</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['p_previa']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Posisi Bayi Sungsang</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['b_sungsang']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Bayi Kembar</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['b_kembar']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Jantung Bayi Lemah</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['b_jantung_lemah']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Fetal Distress</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['fetal_distress']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Giant Baby</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['b_giant']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                    <?php
                        echo '<tr><td>Persentasi Normal</td><td>:</td><td>'.$data['percentage_normal'].'&nbsp;%</td></tr>';
                        echo '<tr><td>Persentasi Sesar</td><td>:</td><td>'.$data['percentage_sesar'].'&nbsp;%</td></tr>';
                  ?>
                        <tr>
                              <td>Hasil</td>
                              <td>:</td>
                              <td>
                                    <?php 
                                          if($data['hasil']=='0') {
                                                echo 'Normal';
                                          }
                                          else {
                                                echo 'Sesar';
                                          }
                                    ?>
                              </td>
                        </tr>
                  <?php
                        echo '<tr><td>Tanggal Checkup</td><td>:</td><td>'.$data['created_at'].'</td></tr>';
                        echo '</table>';
                        echo '<br>';
                        echo '<br>';
                        
                      }
                    }

                ?>
	</div>
</body>
</html>