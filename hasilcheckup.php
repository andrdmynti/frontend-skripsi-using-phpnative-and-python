<?php

	session_start();


?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<style>

		  body {
		      font-family: "Lato", sans-serif;
		  }

		  .sidenav {
		      height: 100%;
		      width: 0;
		      position: fixed;
		      z-index: 1;
		      top: 0;
		      left: 0;
		      background-color: white;
		      overflow-x: hidden;
		      transition: 0.5s;
		      padding-top: 0px;
		  }

		  .sidenav a {
		      padding: 8px 8px 8px 32px;
		      text-decoration: none;
		      font-size: 25px;
		      color: #818181;
		      display: block;
		      transition: 0.3s;
		  }

		  .sidenav a:hover {
		      color: #f1f1f1;
		  }

		  .sidenav .closebtn {
		      position: absolute;
		      top: 0;
		      right: 25px;
		      font-size: 36px;
		      margin-left: 50px;
		  }

		  @media screen and (max-height: 450px) {
		    .sidenav {padding-top: 15px;}
		    .sidenav a {font-size: 18px;}
		  }
		  </style>
		  <style type="text/css">
		    * {
		      padding:0;
		      margin:0;
		    }

		    body {
		      font-family:Verdana, Geneva, sans-serif;
		      font-size:18px;
		      background-color:#FFF
		    }

		    input.untukInput1 {
		      border-bottom: 1px solid #2b2a2a;
		      border-left:none;
		      border-right:none;
		      border-top:none;
		    }

		    header {
		      width:100%;
		      background-color:#006faa ;
		      z-index:1000;
		    }

		    .menu-bar {
		      color:white;
		      font-size:25px;
		      cursor:pointer;
		      padding:10px 12px;
		      margin-left:10px;
		      margin-top:5px;
		      margin-bottom:5px;
		    }

		    .menu-bar:hover {
		      background-color:rgba(0, 0, 0, 0.1);
		      border-radius:50px;
		    }

		    #tag-menu {
		      display:none;
		    }


		    #tag-menu:checked ~ div.jw-drawer {
		     animation: slide-in 0.5s ease;
		     animation-fill-mode: forwards;
		    }

		    .jw-drawer {
		      position:fixed;
		      left:-280px;
		      background-color:#006faa;
		      height:100%;
		      z-index:100;
		      width:230px;
		      animation: slide-out 0.5s ease;
		      animation-fill-mode: forwards;
		    }

		    .jw-drawer ul li {
		      list-style:none;
		    }

		    .jw-drawer ul li a {
		      padding:10px 20px;
		      text-decoration:none;
		      display:block;
		      color:#FFF;
		    }

		    .jw-drawer ul li a:hover{
		      background-color:rgba(0, 0, 0, 0.1);
		    }

		    .jw-drawer ul li a i {
		      width:50px;
		      height:35px;
		      text-align:center;
		      padding-top:15px;
		    }

		    @keyframes slide-in {
		     from {left: -280px;}
		     to {left: 0;}
		    }

		    @keyframes slide-out {
		     from {left: 0;}
		     to {left: -280px;}
		    }
		</style>
	</head>
	<body>
		<header>
			<input type="checkbox" id="tag-menu"/>
			<label onclick="openNav()"  class="fa fa-bars menu-bar" for="tag-menu"></label><font color="white">Riwayat Hasil Checkup</font>

			<div id="mySidenav" class="sidenav">
				<header>
				  <br>
				  <br>
				  <center>
				    <img src="images/bidandelima.png" height="70" width="70">
				  </center>
				  <font size="2" color="white">
				      <br>
				      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hai! Bumil <?php echo $_SESSION['name']; ?>
				      <br>
				      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $_SESSION['email']; ?>
				      <br>
				  </font>
				  <br>
				</header>
			    <br>
			    <a href="datadiripasien.php"><i class="fa fa-user"></i>&nbsp;&nbsp;<font size="2">Data Diri</font></a>
			    <a href="hasilcheckup.php"><i class="fa fa-file"></i>&nbsp;&nbsp;<font size="2">Hasil Checkup</font></a>
			    <a href="artikel.php"><i class="fa fa-newspaper-o"></i>&nbsp;&nbsp;<font size="2">Artikel</font></a>
			    <a href="logout.php"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;<font size="2">Logout</font></a>
			</div>
		  </div>
		</header>

	<div class="container">
	<br>

	<p>Nama : <?php echo $_SESSION['name']; ?>
	<?php
		$nama 	 	= $_SESSION['name'];
		// echo $nama;
		include 'koneksi.php';

		$select  = "SELECT * FROM pasien WHERE nama_pasien='$nama'";
		$query   = mysqli_query($conn,$select)or die(mysqli_error($conn));
		$tampil	 = mysqli_fetch_array($query);

		$id 		= $tampil['id'];
		$umur		= $tampil['umur_pasien'];

	?>
	<br>
	Umur : <?php echo $umur ?>
	</p>
		<br>
			<?php
			include 'koneksi.php';
		  	$querydata = mysqli_query($conn, "SELECT * FROM rekamedis WHERE pasien_id='$id'")or die(mysqli_error($conn));
                    if(mysqli_num_rows($querydata) == 0){
                      echo '<table><tr><td colspan="5" align="center">Tidak ada data!</td></tr></table>';
                    }
                      else
                    {
                      // $no = 1;
                      while($data = mysqli_fetch_array($querydata)){
                        echo '<table>';
                        echo '<tr><td width="300">Minggu Ke-</td><td width="20">:</td><td>'.$data['minggu_ke'].'</td></tr>';
                        echo '<tr><td>Tekanan Darah</td><td>:</td><td>'.$data['sistol'].'/'.$data['diastol'].'</td></tr>';
                        ?>
                        <tr>
                        	<td>Penyakit Menular</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['penyakit_menular']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Mata Minus</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['mata_minus']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Asma</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['asma']=='0') {
                        				echo 'Tidak Mengidap Asma';
                        			}
                        			if($data['asma']=='1') {
                        				echo 'Asma Ringan Berkala';
                        			}
                        			elseif($data['asma']=='2'){
                        				echo 'Asma Ringan Menetap';
                        			}
                        			elseif($data['asma']=='3'){
                        				echo 'Asma Sedang Menetap';
                        			}
                        			else {
                        				echo 'Asma Sedang Menetap';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Jantung</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['jantung']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Hipertensi</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['hipertensi']=='0') {
                        				echo 'Tidak Hipertensi';
                        			}
                        			elseif($data['hipertensi']=='1') {
                        				echo 'Hipertensi Ringan';
                        			}
                        			elseif($data['hipertensi']=='2'){
                        				echo 'Hipertensi Sedang';
                        			}
                        			else {
                        				echo 'Hipertensi Berat';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Diabetes</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['diabetes']=='0') {
                        				echo 'Tidak Diabetes';
                        			}
                        			elseif($data['diabetes']=='1') {
                        				echo 'Diabetes Tipe 1';
                        			}
                        			else {
                        				echo 'Diabetes Tipe 2';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Sesar</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['sesar']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Pinggul Kecil</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['pinggul']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Plasenta Previa</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['p_previa']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Posisi Bayi Sungsang</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['b_sungsang']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Bayi Kembar</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['b_kembar']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Jantung Bayi Lemah</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['b_jantung_lemah']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Fetal Distress</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['fetal_distress']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                        <tr>
                        	<td>Giant Baby</td>
                        	<td>:</td>
                        	<td>
                        		<?php 
                        			if($data['b_giant']=='0') {
                        				echo 'Tidak';
                        			}
                        			else {
                        				echo 'Iya';
                        			}
                        		?>
                        	</td>
                        </tr>
                    <?php
                        echo '<tr><td>Persentasi Normal</td><td>:</td><td>'.$data['percentage_normal'].'</td></tr>';
                        echo '<tr><td>Persentasi Sesar</td><td>:</td><td>'.$data['percentage_sesar'].'</td></tr>';
                        echo '<tr><td>Hasil</td><td>:</td><td>'.$data['hasil'].'</td></tr>';
                        echo '<tr><td>Created At</td><td>:</td><td>'.$data['created_at'].'</td></tr>';
                        echo '</table>';
                        echo '<br>';
                        echo '<br>';
                        
                      }
                    }

                ?>

	</div>
</body>
</html>
<script>
	function closeNav() {
	    document.getElementById("mySidenav").style.width = "0";
	}

	function openNav() {
	    document.getElementById("mySidenav").style.width = "250px";
	    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
	}
</script>