-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 07 Agu 2018 pada 09.32
-- Versi server: 10.1.32-MariaDB
-- Versi PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bidan`
--

CREATE TABLE `bidan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nib` int(11) NOT NULL,
  `nama_bidan` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `tempat_lahir` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_lahir` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `bidan`
--

INSERT INTO `bidan` (`id`, `nib`, `nama_bidan`, `tempat_lahir`, `tgl_lahir`, `alamat`, `status`, `created_at`, `updated_at`) VALUES
(1, 2123123476, 'Muzayanah', 'Bekasi', '1995-01-15', 'Perumahan Duta Kranji, Bekasi Barat, Bekasi', 1, '2018-08-06 02:27:25', '2018-08-06 02:27:25'),
(2, 2123123476, 'Makrifatul', 'Bekasi', '1995-05-06', 'Bintara IV, Bekasi Barat', 1, '2018-08-06 07:58:52', '2018-08-06 07:58:52'),
(3, 987898890, 'Diana', 'Jember', '1990-09-03', 'Harapan Jaya', 1, '2018-08-06 08:03:12', '2018-08-06 08:03:12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contents`
--

CREATE TABLE `contents` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `news` text COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `contents`
--

INSERT INTO `contents` (`id`, `title`, `news`, `picture`, `created_at`, `updated_at`) VALUES
(1, '7 Latihan Mudah Dalam Rangka Persiapan Melahirkan Normal', 'Menghadapi proses melahirkan normal, butuh persiapan fisik. Melahirkan secara normal membutuhkan stamina dan mental yang kuat karena cukup menguras tenaga. Persiapan melahirkan normal agar fisik Ibu kuat bisa dilakukan sendiri di rumah. Berikut beberapa latihan fisik yang bisa Ibu lakukan untuk persiapan melahirkan normal.\r\n1. Latihan Pernapasan\r\nSaat melahirkan normal, hal yang paling penting dilakukan adalah mengatur nafas dengan baik. Jadi bila ingin proses melahirkan berjalan dengan lancar, segera lakukan latihan pernapasan sesering mungkin. Mulai dari atur pernapasan saat sedang berbaring, atau berdiri dan divariasikan dengan gerakan tangan atau kaki. Lakukan setiap hari agar Ibu siap menjalani persalinan.\r\n2. Perkuat Otot Panggul dan Paha.\r\nPersiapan melahirkan normal lainnya adalah memperkuat otot panggul dan paha, dua otot yang digunakan saat bersalin. Caranya bisa dengan berjongkok, lalu gerakkan lutut dan paha perlahan. Hal ini juga bisa membantu posisi bayi berada tepat di jalur jalan lahir. Lakukan di pagi hari selama setiap hari selama 5-10 menit.\r\n3. Latihan Kegel\r\nLatihan kegel atau melatih otot-otot area kewanitaan juga penting dilakukan sebelum melahirkan. Latihan kegel membantu melatih otot agar siap menjalani persalinan, sekaligus juga mencegah wasir dan pembekuan darah atau terjadinya robekan. Latihan kegel membantu Ibu mengontrol otot-otot daerah kewanitaan, apalagi bisa dilakukan dimana saja. Caranya, saat sedang duduk tarik otot area otot kewanitaan, tahan selama beberapa detik lalu lepaskan. Ulangi beberapa kali sehari untuk hasil yang lebih maksimal.\r\nSenam Punggung dan Kaki\r\nTak hanya panggul saja yang perlu diperkuat sebelum melahirkan, tapi juga punggung dan kaki. Saat menjelang persalinan, otot punggung Ibu biasanya akan nyeri karena menahan beban. Lakukan latihan punggung dan panggul, yaitu tempatkan tubuh dalam posisi merangkak, bertumpu pada siku dan lutut. Kemudian tarik otot bokong dan lepaskan. Ulangi 10 kali setiap hari.\r\nSelanjutnya, lakukan latihan untuk memperkuat otot-otot kaki dan paha. Duduk bersila, lalu gerakkan lutut naik turun. Lakukan setiap hari agar semakin siap menuju persalinan.\r\nLatihan-latihan ini bisa dilakukan sejak usia kehamilan sudah 30 minggu. Persiapan melahirkan normal perlu dilakukan secara rutin dan teratur agar persalinan lancar tanpa hambatan', '5b682ad51a9a5.jpg', '2018-08-06 04:02:45', '2018-08-06 04:02:45'),
(2, 'Kontraindikasi Melahirkan Normal', 'Berikut ini merupakan sejumlah kondisi yang tidak memungkinkan bagi seorang ibu untuk melahirkan normal:\r\n\r\n1. Prolaps tali pusat.\r\nKondisi di mana tali pusat menutupi jalan lahir, baik mendahului bagian tubuh janin, maupun bersamaan dengan keluarnya janin. Kondisi ini berisiko menimbulkan penekanan pada tali pusat dan mengakibatkan kematian janin.\r\n2. Kelainan posisi janin.\r\nPada umumya, posisi janin yang normal saat akan dilahirkan adalah puncak kepala berhadapan dengan bukaan leher rahim (serviks), dengan posisi wajah menghadap ke depan atau ke belakang (presentasi kepala). Beberapa kelainan posisi janin yang tidak dianjurkan untuk melahirkan secara normal, antara lain:\r\nPresentasi wajah, ketika kepala janin sangat dengak dan wajah janin yang berhadapan langsung dengan bukaan serviks.\r\n3. Presentasi alis, yaitu ketika posisi kepala janin sedikit mendongak sehingga alis janin berhadapan langsung dengan bukaan serviks. Keadaan ini dapat berubah menjadi presentasi kepala atau presentasi wajah.\r\n4. Presentasi bokong\r\nyaitu ketika bokong berhadapan langsung dengan bukaan serviks, terlebih bila kaki yang berhadapan langsung dengan bukaan serviks.\r\nLetak lintang.\r\n5. Kehamilan kembar.\r\nKehamilan kembar yang tidak boleh melahirkan normal adalah ketika keduanya berada dalam presentasi bokong, kembar siam, terdapat dalam 1 selaput air ketuban, atau kehamilan kembar lebih dari 2 janin.\r\n6. Pernah melakukan operasi caesar.\r\nWalaupun tergolong aman untuk kebanyakan kasus, tetapi hal ini masih menjadi kontroversi. Namun yang pasti, ibu yang pernah melakukan operasi caesar lebih dari 2 kali, atau memiliki bekas luka membujur atau membentuk huruf T pada rahim akibat operasi caesar sebelumnya, tidak boleh melahirkan secara normal, karena berisiko menimbulkan robeknya rahim (ruptur uteri).\r\n7. Denyut jantung janin tidak stabil.\r\nHal ini dapat menjadi tanda janin mengalami hipoksia. Beberapa hal yang dapat mengakibatkan hipoksia pada janin antara lain solusio plasenta atau lilitan tali pusat.\r\n8. Kelainan plasenta.\r\nSeperti letak plasenta yang menutupi jalan lahir (plasenta previa), atau placenta yang menempel sampai ke dalam otot rahim (plasenta akreta).\r\n9. Makrosomia.\r\nKondisi di mana berat badan bayi melebihi 4-4,5 kg, karena berisiko bahu janin terjepit saat proses persalinan (distosia bahu).\r\n10. Ibu yang terinfeksi herpes genital atau HIV.\r\nIbu tidak diperkenankan untuk melahirkan normal bila mengalami infeksi herpes genital yang aktif atau terinfeksi HIV tanpa pengobatan.', '5b68547c5850b.jpg', '2018-08-06 07:00:28', '2018-08-06 07:00:28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_06_11_144024_create_contents_table', 1),
(4, '2018_06_11_144046_create_bidan_table', 1),
(5, '2018_06_11_152327_create_pasien_table', 1),
(6, '2018_06_17_165132_create_rekamedis_table', 1),
(7, '2018_07_03_073921_update_bidan_table_001', 1),
(8, '2018_07_25_105730_update_bidan_table_002', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pasien`
--

CREATE TABLE `pasien` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_pasien` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `tmpt_lahir` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_lahir` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `umur_pasien` int(11) NOT NULL,
  `gol_darah` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8_unicode_ci NOT NULL,
  `no_identitas` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `nama_wali` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `nohp_wali` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `pasien`
--

INSERT INTO `pasien` (`id`, `nama_pasien`, `tmpt_lahir`, `tgl_lahir`, `umur_pasien`, `gol_darah`, `alamat`, `no_identitas`, `nama_wali`, `nohp_wali`, `created_at`, `updated_at`) VALUES
(1, 'Siwi', 'Bekasi', '1995-10-20', 23, 'AB', 'Perumnas 1, Bekasi', '2123456789', 'Onay', '0897689692', '0000-00-00 00:00:00', NULL),
(2, 'Novia', 'Bekasi', '1995-11-30', 23, 'O', 'Harapan Baru', '1234123412', 'Yulio', '0888123123', '2018-08-06 07:52:43', '2018-08-06 07:52:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rekamedis`
--

CREATE TABLE `rekamedis` (
  `id` int(10) UNSIGNED NOT NULL,
  `pasien_id` int(10) UNSIGNED NOT NULL,
  `bidan_id` int(10) UNSIGNED NOT NULL,
  `sistol` text COLLATE utf8_unicode_ci NOT NULL,
  `diastol` text COLLATE utf8_unicode_ci NOT NULL,
  `minggu_ke` int(11) NOT NULL,
  `penyakit_menular` int(11) NOT NULL,
  `mata_minus` int(11) NOT NULL,
  `asma` int(11) NOT NULL,
  `jantung` int(11) NOT NULL,
  `hipertensi` int(11) NOT NULL,
  `diabetes` int(11) NOT NULL,
  `sesar` int(11) NOT NULL,
  `pinggul` int(11) NOT NULL,
  `p_previa` int(11) NOT NULL,
  `b_sungsang` int(11) NOT NULL,
  `b_kembar` int(11) NOT NULL,
  `b_jantung_lemah` int(11) NOT NULL,
  `fetal_distress` int(11) NOT NULL,
  `b_giant` int(11) NOT NULL,
  `percentage_sesar` int(11) NOT NULL,
  `percentage_normal` int(11) NOT NULL,
  `hasil` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `rekamedis`
--

INSERT INTO `rekamedis` (`id`, `pasien_id`, `bidan_id`, `sistol`, `diastol`, `minggu_ke`, `penyakit_menular`, `mata_minus`, `asma`, `jantung`, `hipertensi`, `diabetes`, `sesar`, `pinggul`, `p_previa`, `b_sungsang`, `b_kembar`, `b_jantung_lemah`, `fetal_distress`, `b_giant`, `percentage_sesar`, `percentage_normal`, `hasil`, `created_at`, `updated_at`) VALUES
(57, 1, 3, '120', '80', 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, '2018-08-06 17:00:00', NULL),
(58, 1, 2, '110', '80', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 39, 61, 0, '2018-08-06 17:00:00', NULL),
(59, 1, 3, '120', '80', 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 39, 61, 0, '2018-08-06 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Rian', 'admin', 'rian@gmail.com', '$2y$10$gmo1tfIujDz5rtBfL.W9c.pdYjoHOvGJQTRDDJqcDFWq3nv4oyTsW', 1, NULL, NULL, NULL),
(2, 'Muzayanah', 'Muzayanah', 'moza15@gmail.com', 'f8844f94e39e41fc99355d98f1c70a2b', 2, NULL, NULL, NULL),
(3, 'Siwi', 'Siwi', 'siwi@gmail.com', 'c4b04741d1002e125f031b3cbf6fa298', 3, NULL, '0000-00-00 00:00:00', NULL),
(4, 'Novia', 'Novia', 'opia@gmail.com', 'c222ebd763886a5c0e9fe1e53d0ae0de', 3, NULL, NULL, NULL),
(5, 'Makrifatul', 'Makrifatul', 'makrifatul@gmail.com', '30aa2011c63063456c17c14458510426', 2, NULL, NULL, NULL),
(6, 'Vivi', 'Vivi', 'viviana@gmail.com', 'fc3fe506df6fbd520cb169aefa43b10d', 2, NULL, NULL, NULL),
(7, 'Ryana', 'Ryana', 'iyana@gmail.com', 'f288840a6bb04899834648323dc46b9a', 2, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `bidan`
--
ALTER TABLE `bidan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `rekamedis`
--
ALTER TABLE `rekamedis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rekamedis_pasien_id_foreign` (`pasien_id`),
  ADD KEY `rekamedis_bidan_id_foreign` (`bidan_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `bidan`
--
ALTER TABLE `bidan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `pasien`
--
ALTER TABLE `pasien`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `rekamedis`
--
ALTER TABLE `rekamedis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `rekamedis`
--
ALTER TABLE `rekamedis`
  ADD CONSTRAINT `rekamedis_bidan_id_foreign` FOREIGN KEY (`bidan_id`) REFERENCES `bidan` (`id`),
  ADD CONSTRAINT `rekamedis_pasien_id_foreign` FOREIGN KEY (`pasien_id`) REFERENCES `pasien` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
