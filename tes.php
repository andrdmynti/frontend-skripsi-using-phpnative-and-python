<?php

  session_start();

?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>

  body {
      font-family: "Lato", sans-serif;
  }

  .sidenav {
      height: 100%;
      width: 0;
      position: fixed;
      z-index: 1;
      top: 0;
      left: 0;
      background-color: white;
      overflow-x: hidden;
      transition: 0.5s;
      padding-top: 0px;
  }

  .sidenav a {
      padding: 8px 8px 8px 32px;
      text-decoration: none;
      font-size: 25px;
      color: #818181;
      display: block;
      transition: 0.3s;
  }

  .sidenav a:hover {
      color: #f1f1f1;
  }

  .sidenav .closebtn {
      position: absolute;
      top: 0;
      right: 25px;
      font-size: 36px;
      margin-left: 50px;
  }

  @media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
  }
  </style>
  <style type="text/css">
    * {
      padding:0;
      margin:0;
    }

    body {
      font-family:Verdana, Geneva, sans-serif;
      font-size:18px;
      background-color:#FFF
    }

    header {
      width:100%;
      background-color:#006faa ;
      z-index:1000;
    }

    .menu-bar {
      color:white;
      font-size:25px;
      cursor:pointer;
      padding:10px 12px;
      margin-left:10px;
      margin-top:5px;
      margin-bottom:5px;
    }

    .menu-bar:hover {
      background-color:rgba(0, 0, 0, 0.1);
      border-radius:50px;
    }

    #tag-menu {
      display:none;
    }


    #tag-menu:checked ~ div.jw-drawer {
     animation: slide-in 0.5s ease;
     animation-fill-mode: forwards;
    }

    .jw-drawer {
      position:fixed;
      left:-280px;
      background-color:#006faa;
      height:100%;
      z-index:100;
      width:230px;
      animation: slide-out 0.5s ease;
      animation-fill-mode: forwards;
    }

    .jw-drawer ul li {
      list-style:none;
    }

    .jw-drawer ul li a {
      padding:10px 20px;
      text-decoration:none;
      display:block;
      color:#FFF;
      /*border-top:1px solid #059;*/
    }

    .jw-drawer ul li a:hover{
      background-color:rgba(0, 0, 0, 0.1);
    }

    .jw-drawer ul li a i {
      width:50px;
      height:35px;
      text-align:center;
      padding-top:15px;
    }

    @keyframes slide-in {
     from {left: -280px;}
     to {left: 0;}
    }

    @keyframes slide-out {
     from {left: 0;}
     to {left: -280px;}
    }
  </style>

</head>
<body>
  <header>
    <input type="checkbox" id="tag-menu"/>
    <label onclick="openNav()"  class="fa fa-bars menu-bar" for="tag-menu"></label><font color="white">Data Diri Pasien</font>
    
    <div id="mySidenav" class="sidenav">
      <header>
        <br>
        <br>
        <center>
          <img src="images/user-icon.png" height="70" width="70">
        </center>
        <font size="2" color="white">
            <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selamat Datang Bidan <?php echo $_SESSION['name']; ?>
            <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $_SESSION['email']; ?>
            <br>
        </font>
        <br>
      </header>
      
        <br>
        <a href="datadiripasien.php"><i class="fa fa-user"></i>&nbsp;&nbsp;<font size="2">Data Diri</font></a>
        <a href="hasilcheckup.php"><i class="fa fa-file"></i>&nbsp;&nbsp;<font size="2">Hasil Checkup</font></a>
        <a href="artikel.php"><i class="fa fa-newspaper-o"></i>&nbsp;&nbsp;<font size="2">Artikel</font></a>
        <a href="logout.php"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;<font size="2">Logout</font></a>
    </div>
  </header>
  

  <div class="container">
    <br>
      <table id="data">
      <br>
        <tr>
          <td>
            Nama
          </td>
          <td></td>
          <td>:</td>
          <td> <?php echo $_SESSION['name']; ?></td>
        </tr>
        <?php
          
          $nama = $_SESSION['name'];

          include 'koneksi.php';

          $select  = "SELECT * FROM pasien WHERE nama_pasien='$nama'";
          $query   = mysqli_query($conn,$select)or die(mysqli_error($conn));
          $tampil  = mysqli_fetch_array($query);

        ?>
        <tr><td>Tempat Lahir</td><td></td><td>:</td><td><?php echo $tampil['tmpt_lahir'] ?></td></tr>
        <tr><td>Tanggal Lahir</td><td></td><td>:</td><td><?php echo $tampil['tgl_lahir'] ?></td></tr>
        <tr><td>Umur</td><td></td><td>:</td><td><?php echo $tampil['umur_pasien'] ?></td></tr>
        <tr><td>Golongan Darah</td><td></td><td>:</td><td><?php echo $tampil['gol_darah'] ?></td></tr>
        <tr><td>Alamat</td><td></td><td>:</td><td><?php echo $tampil['alamat'] ?></td>
        <tr><td>No Identitas</td><td></td><td>:</td><td><?php echo $tampil['no_identitas'] ?></td></tr>
        <tr><td>Nama Wali</td><td></td><td>:</td><td><?php echo $tampil['nama_wali'] ?></td></tr>
        <tr><td>No Hp Wali</td><td></td><td>:</td><td><?php echo $tampil['nohp_wali'] ?></td></tr>
      </table>
  </div>
</body>
</html>

<script>


function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}
</script>