# -*- coding: utf-8 -*-
from __future__ import print_function, division
from flask import Flask, jsonify, abort, request, make_response, url_for, render_template


app = Flask(__name__, static_url_path = "/static")


@app.errorhandler(400)
def not_found(error):
    return make_response(jsonify( { 'error': 'Bad request' } ), 400)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify( { 'error': 'Not found' } ), 404)

@app.route('/', methods = ['GET'])
def neuron():
    return render_template('neuron.html')

    a = 7
    b = 5
    
    c = (a + b) / 2

    print(c)
    
    response = {
        'c': str(c)
        
    }

    return jsonify( { 'response': response } )
    
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug = True)